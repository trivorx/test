﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Test {
    public abstract class BaseAction : IFilePathParser {
        protected IEnumerable<string> FilePathList { get; set; }
        public abstract IEnumerable<string> ParseFilePathList();
        public BaseAction(IEnumerable<string> _filePathList) {
            FilePathList = _filePathList;
        }    
    }
    public class Action_all : BaseAction {
        public Action_all(IEnumerable<string> _filePathList) : base(_filePathList) {}

        public override IEnumerable<string> ParseFilePathList() {
            return FilePathList;
        }
    }
    public class Action_cs : BaseAction {
        public Action_cs(IEnumerable<string> _filePathList) : base(_filePathList) { }

        public override IEnumerable<string> ParseFilePathList() {
            var result = FilePathList.ToList();
            result.RemoveAll(x => !x.Contains(".cs"));
            result = result.Select(x => string.Concat(x, "/")).ToList();
            return result;
        }
    }
    public class Action_reversed1 : BaseAction {
        public Action_reversed1(IEnumerable<string> _filePathList) : base(_filePathList) { }

        public override IEnumerable<string> ParseFilePathList() {
            var result = new List<string>();
            foreach(var path in FilePathList.ToList()) {
                result.Add(String.Join("\\", path.Split('\\').Reverse()));
            }

            return result;
        }
    }
    public class Action_reversed2 : BaseAction {
        public Action_reversed2(IEnumerable<string> _filePathList) : base(_filePathList) { }

        public override IEnumerable<string> ParseFilePathList() {
            var result = new List<string>();
            foreach (var path in FilePathList.ToList()) {
                result.Add(String.Join("\\", path.Split('\\').Reverse().Select(x=>String.Join("", x.ToCharArray().Reverse()))));
            }

            return result;
        }
    }
}
