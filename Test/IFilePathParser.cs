﻿using System;
using System.Collections.Generic;

namespace Test {
    interface IFilePathParser {
        IEnumerable<string> ParseFilePathList();
    }
}
