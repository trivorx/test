using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using CommandLine;

namespace Test {
    public class Options {
        [Option('i', "input", Required = true, HelpText = "Start folder to use tool on.")]
        public string InputDir { get; set; }
        [Option('a', "action", Required = true, HelpText = "Action to take on start folder(all/cs/reversed1/reversed2.")]
        public string Action { get; set; }

        [Option('o', "output", Required = false, HelpText = "Output file to write results to(default = results.txt).")]
        public string OutputFilePath { get; set; }
    }
    class Program {
        private static string InputDir { get; set; }
        private static string Action { get; set; }
        private static string OutputFilePath { get; set; } = "results.txt";
        private static List<string> ValidActions { get; } =
            new List<string>() { "all", "cs", "reversed1", "reversed2" };
        private static List<string> FilePathList { get; set; } = new List<string>();


        public static int Main(string[] args) {
            Parser.Default.ParseArguments<Options>(args)
                .WithParsed<Options>(o => {
                    InputDir = o.InputDir;
                    Action = o.Action;
                    if (!String.IsNullOrEmpty(o.OutputFilePath)) {
                        OutputFilePath = o.OutputFilePath;
                    }
                });

            //check validity of args
            if (!File.GetAttributes(InputDir).HasFlag(FileAttributes.Directory)) {
                Console.WriteLine("InputDir must be a valid directory.");
                return -1;
            }
            if (!ValidActions.Contains(Action)) {
                Console.WriteLine("Action must be one of all/cs/reversed1/reversed2.");
                return -2;
            }
            try {
                File.Create(OutputFilePath).Close();
            } catch (Exception e) {
                Console.WriteLine(e.Message);
                return -3;
            }

            Task getDirsTask = Task.Factory.StartNew(() => {
                FilePathList.AddRange(Directory.GetFiles(InputDir, "*.*", SearchOption.AllDirectories));
                FilePathList = FilePathList.Select(x => x.Remove(0, InputDir.Length + 1)).ToList();
            });
            Console.WriteLine("Traversing directory structure. Press Q to cancel...");
            while (true) {
                if (getDirsTask.IsCompleted)
                    break;
                if (Console.KeyAvailable) {
                    var key = Console.ReadKey(true);
                    if (key.Key == ConsoleKey.Q) {
                        Console.WriteLine("Operation interrupted by user.");
                        return -3;
                    }
                }
                Thread.Sleep(10);
                
            }
            var result = new List<string>();
            switch (Action) {
                case "all":
                    result = new Action_all(FilePathList).ParseFilePathList().ToList();
                    break;
                case "cs":
                    result = new Action_cs(FilePathList).ParseFilePathList().ToList();
                    break;
                case "reversed1":
                    result = new Action_reversed1(FilePathList).ParseFilePathList().ToList();
                    break;
                case "reversed2":
                    result = new Action_reversed2(FilePathList).ParseFilePathList().ToList();
                    break;
            }
            File.WriteAllLines(OutputFilePath, result);
            //Console.ReadKey();
            return 0;
        }


    }
}
