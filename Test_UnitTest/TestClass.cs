﻿using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Test;

namespace Test_UnitTest {
    [TestClass]
    public class TestClass {
        
        private List<string> TestList = new List<string>() {
            @"f\bla\ra\t.dat",
            @"C\temp\wood.exe",
            @"usr\bin\re.cs"
        };
        [TestMethod]
        public void TestActionAll() {
            var test = new Action_all(TestList).ParseFilePathList().ToList();
            CollectionAssert.AreEqual(test, TestList);
        }
        [TestMethod]
        public void TestActionCs() {
            var test = new Action_cs(TestList).ParseFilePathList().ToList();
            CollectionAssert.AreEqual(test, new List<string>() { @"usr\bin\re.cs/" });
        }
        [TestMethod]
        public void TestActionReversed1() {
            var test = new Action_reversed1(TestList).ParseFilePathList().ToList();
            CollectionAssert.AreEqual(test, new List<string>() {
                @"t.dat\ra\bla\f",
                @"wood.exe\temp\C",
                @"re.cs\bin\usr"
            });
        }
        [TestMethod]
        public void TestActionReversed2() {
            var test = new Action_reversed2(TestList).ParseFilePathList().ToList();
            CollectionAssert.AreEqual(test, new List<string>() {
                @"tad.t\ar\alb\f",
                @"exe.doow\pmet\C",
                @"sc.er\nib\rsu"
            });
        }
    }
}
